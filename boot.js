let log = require('@dosarrest/loopback-component-logger')('perspective-admin/boot');
// let cascadeDelete = require('loopback-cascade-delete-mixin/cascade-delete');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  init() {
    let me = this;
    let done = me.done;
    // let app = me.app;
    // let ACL = app.registry.getModelByType('ACL');
    // let User = app.registry.getModelByType('User');
    // try {
    //   let AdminNotification = app.registry.getModelByType('AdminNotification');
    //   User.hasMany(AdminNotification, {as: 'adminNotifications', foreignKey: 'userId'});
    //   User.nestRemoting('adminNotifications');
    //   ACL.create({
    //     model: User.definition.name,
    //     accessType: 'READ',
    //     principalType: 'ROLE',
    //     principalId: '$owner',
    //     permission: 'ALLOW',
    //     property: '__get__adminNotifications'
    //   });
    //   AdminNotification.belongsTo(User, {as: 'user', foreignKey: 'userId'});
    //   cascadeDelete(User, {
    //     relations: ["adminNotifications"]
    //   });
    // } catch (err) {
    //   log.debug(err);
    //   log.info('@extlb/module-notifications not installed');
    // }
    done();
  }
}
module.exports = Boot;
