Ext.define('Client.view.perspective.admin.center.View', {
	extend: 'Ext.Container',
	xtype: 'perspective.admin.center',
	cls: 'perspective-admin-center',
	layout: 'card'
});
