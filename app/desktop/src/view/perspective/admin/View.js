Ext.define('Client.view.perspective.admin.View', {
	extend: 'Ext.Container',
	xtype: 'perspective.admin',
	controller: {type: 'perspective.admin'},
	viewModel: {type: 'perspective.admin'},
  requires: ['Ext.layout.Fit'],
	layout: 'fit',
  listeners: {
	  initialize: 'onMainViewInitialize'
  },
	items: [
		{ xtype: 'perspective.admin.navigation',    reference: 'navview',    docked: 'left',   bind: {width:  '{navview_width}'}, listeners: { select: "onMenuViewSelectionChange"} },
		{ xtype: 'perspective.admin.header', reference: 'headerview', docked: 'top',    bind: {height: '{headerview_height}', hidden: '{!showHeader}'} },
		{ xtype: 'perspective.admin.footer', reference: 'footerview', docked: 'bottom', bind: {height: '{footerview_height}', hidden: '{!showFooter}'} },
		{ xtype: 'perspective.admin.center', reference: 'centerview' },
	]
});
