Ext.define('Client.view.admin.ViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.perspective.admin',
  requires: [
    'Client.model.perspective.admin.NavigationItem'
  ],
	data: {
		name: 'Client',
    apiUrl: '/api/v1',
    showHeader: true,
    showFooter: false,
		navCollapsed:       false,
    notificationsCollapsed: false,
		navview_max_width:    300,
		navview_min_width:     60,
		topview_height:        75,
		bottomview_height:     50,
		headerview_height:     50,
		footerview_height:     50,
    user: {
		  username: 'Username',
    },
    menuExtraParams: {
      filter: '{"order": ["weight ASC", "text ASC"]}'
    },
	},
	formulas: {
		navview_width: function(get) {
			return get('navCollapsed') ? get('navview_min_width') : get('navview_max_width');
		}
	},
	stores: {
		menu: {
		  autoLoad: false,
			type: 'tree',
      model: 'Client.model.perspective.admin.NavigationItem',
      proxy: {
			  type: 'rest',
        url: '{apiUrl}',
        extraParams: '{menuExtraParams}',
        reader: {
			    type: 'json'
        }
      },
			root: {
			  id: 'AdminNavigation',
				expanded: true,
				children: []
			}
		}
	}
});
