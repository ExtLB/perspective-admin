Ext.define('Client.view.admin.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.perspective.admin',

	routes: {
		'admin(/:{routeId})(/:{arg0})(/:{arg1})(/:{arg2})': {action: 'adminRoute'}
	},
	adminRoute:function(params) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    let Loader = Client.app.getController('Loader');
    let Authentication = null;
    let user = null;
    let adminRole = null;
    if (Loader.serverInfo.auth.enabled === true) {
      Authentication = Client.app.getController('Authentication');
      user = Authentication.user;
      adminRole = _.find(user.roles, (role) => { return role.name === 'Administrator'; });
    }
    if (Loader.serverInfo.auth.enabled === false || (Authentication && Authentication.loggedIn === true && !_.isUndefined(adminRole))) {
      let user = vm.get('user');
      try {
        if (Loader.serverInfo.auth.enabled === true) {
          if (user.id !== Authentication.user.id) {
            vm.set('user', Authentication.user);
          }
        }
      } catch (err) {
        console.error(err);
      }
      let routeId = params.routeId;
      try {
        // console.log('Main Route Executing');
        //var menuview = this.lookup('menuview');
        let navview = me.lookup('navview');
        let menuview = navview.items.items[0];

        let centerview = me.lookup('centerview');
        if (_.isUndefined(routeId)) {
          console.log('Route Id is undefined');
        } else {
          console.log('Route Id is defined as', routeId);
        }

        let menuStore = menuview.getStore();
        function navigate() {
          let node = menuStore.findNode('routeId', routeId);
          if (node == null) {
            console.log('unmatchedRoute: ' + routeId);
            return;
          }
          let exists = Ext.ClassManager.getByAlias('widget.admin.' + node.get('viewType'));
          if (exists === undefined) {
            console.log(routeId + ' does not exist');
            return;
          }
          if (!centerview.getComponent(routeId)) {
            centerview.add({xtype: `admin.${node.get('viewType')}`, itemId: routeId, heading: node.get('text')});
          }
          centerview.setActiveItem(centerview.getComponent(routeId));
          menuview.setSelection(node);
          let vm = me.getViewModel();
          vm.set('heading', [node.get('text')]);
        }
        if (menuStore.isLoaded() === false || menuStore.isLoading() === true || menuStore.root.hasChildNodes() === false) {
          menuStore.on('load', () => {
            if (_.isUndefined(routeId) && menuStore.count() > 0) {
              routeId = menuStore.first().get('routeId');
              me.redirectTo(`admin/${routeId}`);
            } else {
              navigate();
            }
          }, me, {
            single: true
          });
          if (menuStore.isLoading() === false && menuStore.root.get('loading') === false) {
            menuStore.load();
          }
        } else {
          if (_.isUndefined(routeId) && menuStore.count() > 0) {
            routeId = menuStore.first().get('routeId');
            me.redirectTo(`admin/${routeId}`);
          } else {
            navigate();
          }
        }
      } catch (err) {
        console.error(err);
      }
    } else {
      if (Loader.serverInfo.auth.loginUrl.startsWith('#')) {
        Client.app.originalHash = window.location.hash.substring(1);
        me.redirectTo(Loader.serverInfo.auth.loginUrl.substr(1));
      } else {
        window.location.href = Loader.serverInfo.auth.loginUrl;
      }
    }
	},

	onMenuViewSelectionChange: function (tree, node) {
		if (node == null) { return }
		var vm = this.getViewModel();
		if (node.get('routeId') !== undefined) {
			this.redirectTo( `admin/${node.get('routeId')}` );
		}
	},

	onTopViewNavToggle: function () {
		var vm = this.getViewModel();
		vm.set('navCollapsed', !vm.get('navCollapsed'));
	},

  onMainViewInitialize: function () {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    let Loader = Client.app.getController('Loader');
    if (Loader.serverInfo.auth.enabled === true) {
      let Authentication = Client.app.getController('Authentication');
      if (Authentication.loggedIn === true) {
        vm.set('user', Authentication.user);
      }
      Authentication.on('login', () => {
        vm.set('user', Authentication.user);
      });
    }
    Ext.apply(Ext.util.Format, {
      adminHeader: function (value) {
        return _.join(value.map((v) => { return i18next.t(v); }), ' > ');
      },
    });
    if (Client.view.perspective.admin.header.addons) {
      let addons = Ext.Array.sort(Ext.Object.getKeys(Client.view.perspective.admin.header.addons), (a, b) => {
        if (Client.view.perspective.admin.header.addons[a].View !== undefined && Client.view.perspective.admin.header.addons[b].View !== undefined) {
          let aView = Client.view.perspective.admin.header.addons[a].View;
          let bView = Client.view.perspective.admin.header.addons[b].View;
          if (aView.order !== undefined && bView.order !== undefined) {
            return aView.order - bView.order;
          } else if (aView.order !== undefined) {
            return aView.order;
          } else if (bView.order !== undefined) {
            return bView.order;
          } else {
            return 0;
          }
        } else if (Client.view.perspective.admin.header.addons[a].View !== undefined) {
          let aView = Client.view.perspective.admin.header.addons[a].View;
          return aView.order;
        } else if (Client.view.perspective.admin.header.addons[b].View !== undefined) {
          let bView = Client.view.perspective.admin.header.addons[b].View;
          return bView.order;
        }
      });
      let headerView = me.lookup('headerview');
      Ext.each(addons, (addon) => {
        headerView.add(Ext.create(`Client.view.perspective.admin.header.addons.${addon}.View`));
      });
      console.log('Main Header Addons:', addons);
    }
    // setTimeout(() => {
    //   vm.getStore('desktopmenu').load();
    // }, 1);
  },

  onProfileBtn: function (button) {
    // let me = this;
    // let view = me.getView();
    // let vm = view.getViewModel();
    // let user = vm.get('user');
    let dialog = Ext.create('Client.dialog.user.profile.View', {
      user: 'me'
    });
    dialog.showBy(button);
  }

//	onActionsViewLogoutTap: function( ) {
//		var vm = this.getViewModel();
//		vm.set('firstname', '');
//		vm.set('lastname', '');
//
//		Session.logout(this.getView());
//		this.redirectTo(AppCamp.getApplication().getDefaultToken().toString(), true);
//	}

});
