Ext.define('Client.view.perspective.admin.nav.top.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.admin.nav.top',
	cls: 'perspective-admin-nav-top',
	shadow: false,
	items: [
		{
			xtype: 'container',
			cls: 'perspective-admin-nav-toptext',
			bind: {
				html: '{"per-admin:TITLE":translate}',
				hidden: '{navCollapsed}'
			}
		}, '->', {
			xtype: 'button',
			ui: 'perspective-admin-nav-topbutton',
      bind: {
			  tooltip: '{"per-admin:TOP_MENU_BTN_TOOLTIP":translate}'
      },
			reference: 'navtoggle',
			handler: 'onTopViewNavToggle',
			iconCls: 'x-fa fa-bars'
		}
	]
});
