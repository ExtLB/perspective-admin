Ext.define('Client.view.perspective.admin.nav.bottom.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.admin.nav.bottom',
	cls: 'perspective-admin-nav-bottom',
	shadow: false,
	items: [
		{
			xtype: 'button',
			ui: 'perspective-admin-nav-bottombutton',
      padding: '0 0 0 6px',
			iconCls: 'x-fa fa-angle-double-left',
      bind: {
        text: '{"per-admin:LOGOUT":translate}',
        tooltip: '{"per-admin:LOGOUT":translate}',
      },
			handler: 'onBottomViewlogout'
		}
	]
});
