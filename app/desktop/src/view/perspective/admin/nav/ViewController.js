Ext.define('Client.view.admin.nav.NavViewController', {
	extend: "Ext.app.ViewController",
	alias: "controller.perspective.admin.navigation",
  requires: [
    'Ext.Ajax',
    'Ext.util.Cookies'
  ],

	// initViewModel: function(vm) {},

	onTopViewNavToggle: function (btn) {
		var vm = this.getViewModel();
		vm.set('navCollapsed', !vm.get('navCollapsed'));
		btn.setIconCls((vm.get('navCollapsed')?'logo-big':'x-fa fa-bars'));
	},

	onMenuViewSelectionChange: function(tree, node) {
		if (!node) {
				return;
		}
		this.fireViewEvent("select", node);
	},

	onBottomViewlogout: function () {
	  let me = this;
    localStorage.setItem("LoggedIn", false);
    let Authentication = Client.app.getController('Authentication');
    try {
      Client.app.originalHash = window.location.hash.substring(1);
    } catch (err) {
      console.error(err);
    }
    Authentication.logout().then(() => {
      me.redirectTo('auth/login');
    }).catch(err => {
      me.redirectTo('auth/login');
    });
    // let domain = ParseDomain(window.location.hostname);
    // Ext.util.Cookies.set('originApp', `${window.location.protocol}//${window.location.host}/`, null, '/', `${domain.domain}.${domain.tld}`);
    // localStorage.setItem("LoggedIn", false);
    // Ext.Ajax.request({
    //   url: '/api/v1/Users/logout',
    //   method: 'POST'
    // }).then(response => {
    //   Ext.util.Cookies.clear('accessToken');
    //   Ext.util.Cookies.clear('connect.sid');
    //   me.redirectTo('auth/login');
    // }).catch(err => {
    //   Ext.util.Cookies.clear('accessToken');
    //   Ext.util.Cookies.clear('connect.sid');
    //   me.redirectTo('auth/login');
    //   console.error(err)
    // });
		// this.getView().destroy();
		// Ext.Viewport.add([{ xtype: 'loginview'}]);
	},
});
