Ext.define('Client.view.perspective.admin.nav.menu.View', {
	extend: 'Ext.list.Tree',
	xtype: 'perspective.admin.nav.menu',
	ui: 'perspective-admin-navigation',
	requires: [
		'Ext.data.TreeStore',
	],
	scrollable: true,
	bind: {
		store: '{menu}',
		micro: '{navCollapsed}'
	},
	expanderFirst: false,
	expanderOnly: false
});
