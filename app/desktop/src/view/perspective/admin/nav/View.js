Ext.define('Client.view.perspective.admin.navigation.View', {
	extend: 'Ext.Panel',
	xtype: 'perspective.admin.navigation',
	controller: "perspective.admin.navigation",
	cls: 'perspective-admin-navigation',
	layout: 'fit',
	tbar: {xtype: 'perspective.admin.nav.top', bind: { height: '{headerview_height}' }},
	items: [
		{
			xtype: 'perspective.admin.nav.menu',
			reference: 'menuview',
			bind: {width: '{navview_width}'},
			listeners: {
				selectionchange: "onMenuViewSelectionChange"
			}
		}
	],
	bbar: {xtype: 'perspective.admin.nav.bottom', bind: {height: '{bottomview_height}'}}
});
