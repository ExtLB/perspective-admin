Ext.define('Client.view.perspective.admin.header.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.admin.header',
	cls: 'perspective-admin-header',
  requires: [
    'Ext.XTemplate',
    'Ext.dataview.plugin.ListPaging',
    'Ext.Img'
  ],
	items: [
		{
			xtype: 'container',
			cls: 'perspective-admin-headertext',
			bind: { html: '{heading:adminHeader}' }
		}, '->', {
	    xtype: 'container',
      reference: 'headerBtns',
      padding: 5,
      items: [],
    }
	]
});
