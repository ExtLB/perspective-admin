Ext.define('Client.view.perspective.admin.footer.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.admin.footer',
	cls: 'perspective-admin-footer',

	items: [
		{
			xtype: 'container',
			cls: 'perspective-admin-footertext',
      bind: {
        html: '{"per-admin:FOOTER":translate}'
      }
		},
//		'->',
//		{
//			xtype: 'button',
//			ui: 'footerbutton',
//			iconCls: 'x-fa fa-automobile'
//		}
	]
});
