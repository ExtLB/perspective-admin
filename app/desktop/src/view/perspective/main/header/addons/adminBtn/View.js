Ext.define('Client.view.perspective.main.header.addons.adminBtn.View', {
  extend: 'Ext.Container',
  controller: {type: 'perspective.main.header.addons.adminBtn'},
  viewModel: {type: 'perspective.main.header.addons.adminBtn'},
  padding: 5,
  listeners: {
    initialize: 'onAdminBtnInitialize'
  },
  items: [{
    xtype: 'button',
    ui: 'round raised',
    bind: {
      tooltip: '{"per-admin:ADMINISTRATION":translate}',
      hidden: '{!isAdmin}'
    },
    reference: 'adminBtn',
    handler: 'onAdminBtn',
    iconCls: 'x-fa fa-cogs',
  }]
});
Client.view.perspective.main.header.addons.adminBtn.View.addStatics({
  order: 1
});
