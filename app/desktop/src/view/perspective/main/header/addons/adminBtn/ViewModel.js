Ext.define('Client.view.perspective.main.header.addons.adminBtn.ViewModel', {
  extend: 'Ext.app.ViewModel',
  alias: 'viewmodel.perspective.main.header.addons.adminBtn',
  data: {
    user: {},
    isAdmin: false,
  }
});
