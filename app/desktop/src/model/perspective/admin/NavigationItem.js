Ext.define('Client.model.perspective.admin.NavigationItem', {
  extend: 'Ext.data.Model',

  requires: [
    'Ext.data.field.Field'
  ],

  fields: [{
    name: 'id',
    persist: false
  }, {
    name: 'text',
    convert: (value) => {
      return i18next.t(value);
    }
  }, {
    name: 'iconCls'
  }, {
    name: 'rowCls'
  }, {
    name: 'viewType'
  }, {
    name: 'routeId'
  }, {
    name: 'leaf'
  }, {
    name: 'expanded'
  }, {
    name: 'selectable'
  }, {
    name: 'children'
  }]

});
