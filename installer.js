let config = require('./extlb');
let path = require('path');
let shelljs = require('shelljs');
let _ = require('lodash');
module.exports = function (configs) {
  if (configs['model-config.json']._meta.sources.indexOf(`../node_modules/${config.name}/common/models`) === -1) {
    configs['model-config.json']._meta.sources.push(`../node_modules/${config.name}/common/models`);
  }
  if (configs['model-config.json']._meta.sources.indexOf(`../node_modules/${config.name}/server/models`) === -1) {
    configs['model-config.json']._meta.sources.push(`../node_modules/${config.name}/server/models`);
  }
  if (configs['model-config.json']._meta.mixins.indexOf(`../node_modules/${config.name}/common/mixins`) === -1) {
    configs['model-config.json']._meta.mixins.push(`../node_modules/${config.name}/common/mixins`);
  }
  if (configs['model-config.json']._meta.mixins.indexOf(`../node_modules/${config.name}/server/mixins`) === -1) {
    configs['model-config.json']._meta.mixins.push(`../node_modules/${config.name}/server/mixins`);
  }
  let modelConfigFile = `${__dirname}${path.sep}server${path.sep}model-config.json`;
  if (shelljs.test('-e', modelConfigFile)) {
    let modelConfig = require(modelConfigFile);
    if (_.isObject(modelConfig) && _.keys(modelConfig).length > 0) {
      configs['model-config.json'] = _.merge(configs['model-config.json'], modelConfig);
    }
  }
  return configs;
};
